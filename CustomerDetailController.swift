//
//  CustomerDetailController.swift
//  Vyomo
//
//  Created by apple on 03/04/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class CustomerDetailController: UIViewController, UITableViewDelegate, UIScrollViewDelegate , UIPickerViewDelegate {
   
    @IBOutlet var annYearLabel: UILabel!
    @IBOutlet var annMonthLabel: UILabel!
    @IBOutlet var annDateLabel: UILabel!
    @IBOutlet var birthYearLabel: UILabel!
    @IBOutlet var birthMonthLabel: UILabel!
    @IBOutlet var birthDateLabel: UILabel!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBAction func editName(sender: AnyObject) {
    }
    @IBAction func phoneCall(sender: AnyObject) {
    }
    @IBAction func whatsapp(sender: AnyObject) {
    }
    @IBAction func email(sender: AnyObject) {
    }
    @IBAction func book(sender: AnyObject) {
    }
    @IBAction func birthDate(sender: AnyObject) {
        pickerViewing.hidden = false
        tableView.hidden = true
        bookOutlet.hidden = true
        upcomingOutlet.hidden = true
        pastOutlet.hidden = true
    }
    @IBAction func birthMonth(sender: AnyObject) {
        pickerViewing.hidden = false
        tableView.hidden = true
        bookOutlet.hidden = true
        upcomingOutlet.hidden = true
        pastOutlet.hidden = true
        
    }
    @IBAction func birthYear(sender: AnyObject) {
        pickerViewing.hidden = false
        tableView.hidden = true
        bookOutlet.hidden = true
        upcomingOutlet.hidden = true
        pastOutlet.hidden = true
    }
    @IBAction func anniversaryDate(sender: AnyObject) {
        pickerViewing.hidden = false
        tableView.hidden = true
        bookOutlet.hidden = true
        upcomingOutlet.hidden = true
        pastOutlet.hidden = true
    }
    @IBAction func anniversaryMoth(sender: AnyObject) {
        pickerViewing.hidden = false
        tableView.hidden = true
        bookOutlet.hidden = true
        upcomingOutlet.hidden = true
        pastOutlet.hidden = true
    }
    @IBAction func anniversaryYear(sender: AnyObject) {
        pickerViewing.hidden = false
        tableView.hidden = true
        bookOutlet.hidden = true
        upcomingOutlet.hidden = true
        pastOutlet.hidden = true
        
    }
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var pickerViewing: UIView!
    @IBAction func donePressed(sender: AnyObject) {
        pickerViewing.hidden = true
        bookOutlet.hidden = false
        upcomingOutlet.hidden = false
        pastOutlet.hidden = false
        past(self)
    }
    
    @IBOutlet var bookOutlet: UIButton!
    @IBOutlet var upcomingOutlet: UIButton!
    
    @IBOutlet var pastOutlet: UIButton!
    
    @IBAction func upcoming(sender: AnyObject) {
        tableView.hidden = true
        upcomingOutlet.backgroundColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        pastOutlet.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
        pickerViewing.hidden = true
    }
    
    @IBAction func past(sender: AnyObject) {
        tableView.hidden = false
        pastOutlet.backgroundColor = UIColor(red: 53/255, green: 63/255, blue: 73/255, alpha: 1)
        upcomingOutlet.backgroundColor = UIColor(red: 224/255, green: 20/255, blue: 79/255, alpha: 1)
        pickerViewing.hidden = true
    }
    @IBAction func backArrow(sender: AnyObject) {
    }
    
    
    
    var accessToken = ""
    var customerId = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        past(self)
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.rowHeight = 98
        self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "CellTable")
        self.profileImage.layer.cornerRadius = self.profileImage.frame.size.width / 2
        
    }
    //MARK: Picker control functions...
    func numberOfComponentsInPickerView(pickerView : UIPickerView) -> Int { // number of components in picker
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int { // number of rows in picker
        return 4
    }
    
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 60
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
        var myView = UIView(frame: CGRectMake(0 , 0, pickerView.bounds.width - 40 , 50))
        var myImageView = UIImageView(frame: CGRectMake(150 , 0, 50, 50))
        
       if pickerView.tag == 0 {
            println("0")
        }
        return myView
    }// picker contains a view , view contains UIImageview , and UIImageview is assigned is UIImage based on picker tag
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        
      if pickerView.tag == 0 {
            println("0")
        }
        return ""
            }
 
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 0 {
            println("entering")
        } else {
            println("enter")
        }
    }// on selecting picker row based on picker tag
    
    
    //MARK: Scroll view functions...
    func scrollViewDidScroll(scrollView: UIScrollView!) {
    }
    
    func scrollViewWillEndDragging(scrollView: UIScrollView!, velocity: CGPoint, inout targetContentOffset: CGPoint) {
    }
    
   
    //MARK: table view functions...
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 10
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cels = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "CellTable")
        
        return cels
    }

    
    
    
    
    func jsonData() {
    var arr_parameters = NSDictionary(objectsAndKeys: "\(accessToken)","YW5kcm9pZF9tYW5hZ2VyU2F0IE1hciAyOCAyMDE1IDA4OjUwOjM4IEdNVCswMDAwIChVVEMp",
        "\(customerId)","2")
    println(arr_parameters)
        
    json.session("get_customer_details", parameters: arr_parameters, completion: {
    result in
    let jsonResult = result as NSDictionary
    let status = jsonResult["status"]! as Int
    let message = jsonResult["message"]! as NSString
    let parsedResult = jsonResult["data"]! as NSDictionary
    var emptyDictionary = NSDictionary()
    
    println("GetdefaultStylistAppointment_today: \(parsedResult)")
    dispatch_async(dispatch_get_main_queue()){
    
    if status == 200 {
    
    if parsedResult ==  emptyDictionary {
    
    println("imhere")
    
    } else {
    
    println("here")
    
        if var birthDate: String = parsedResult["customer_birth_date"] as? String {
            birthDate = birthDate.componentsSeparatedByString("0000-00-")[1] as NSString
            birthDate = birthDate.componentsSeparatedByString(" 00:00:00")[0] as NSString
            println(birthDate)
        }
        if var birthMonth: String = parsedResult["customer_birth_date"] as? String {
            birthMonth = birthMonth.componentsSeparatedByString("0000-")[1] as NSString
            birthMonth = birthMonth.componentsSeparatedByString("-00 00:00:00")[0] as NSString
            println(birthMonth)
        }

        }
        }
        }
    })
    }
   /* func getBuisnessType() {
    
        var url : String = "http://54.173.217.54:8080/get_business_type"
        var request : NSMutableURLRequest = NSMutableURLRequest()
        request.URL = NSURL(string: url)
        request.HTTPMethod = "GET"
    
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
            let jsonResult: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error: error) as? NSDictionary
            
            
            if (jsonResult != nil) {
                //For status..
                if let status: Int = jsonResult["status"] as? Int {
                    if status == 200 {
                        println(jsonResult["message"]!)
                        //For Data..
                        if let data: NSDictionary = jsonResult["data"] as? NSDictionary {
                            if let insideData: NSArray = data["business_type"] as? NSArray {
                                for i:Int in 0..<insideData.count {
                                    if let buisness: NSDictionary = insideData[i]  as? NSDictionary {
                                        if let buisnessId: Int = buisness["business_type_id"] as? Int {
                                            if let buisnessName: String = buisness["business_type_name"] as? String {
                                                println(buisnessName)
                                                println(buisnessId)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else if status == 201 {
                        println(jsonResult["message"])
                        if let data: NSDictionary = jsonResult["data"] as? NSDictionary {
                            if let insideData: NSArray = data["business_type"] as? NSArray {
                                println(insideData)
                            }
                        }
                    }
                    else if status == 404 {
                        println(jsonResult["message"])
                    }
                }
                
                
                
                
                
                
                //For Message...
                if (jsonResult != nil) {
                    if let Message: String = jsonResult["message"] as? String {
                        println(Message)
                    }
                }else {
                    println(error)
                    
                }
                
            }
        })
        /*let manager = AFHTTPRequestOperationManager()
        manager.requestSerializer.setValue("608c6c08443c6d933576b90966b727358d0066b4", forHTTPHeaderField: "X-Auth-Token")
        
        manager.securityPolicy.allowInvalidCertificates = true
        
        var parameterss:NSDictionary = ["email":userInputemail,
            "password":userInputpassword,
            "device_token":myDeviceToken,
            "device_type":"1",
            "device_name":"iPhone",
            "app_version":"101",
            "os_version":systemVersion,
            "country":"India",
            "unique_device_id":"",
            "client_id":clientIdentifier,
            "otp":oneTimepassword]
        
        println("passing parameters")
        println(parameterss)
        // https://54.81.229.172:8012/
        // var parameterss = [“user”:”admin”,”password”:”123456"]
        
        
        manager.POST( "https://www.test.jugnoo.in:8012/verify_otp",
            parameters: parameterss,
            success: {
                (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
                var resultDesp:String = responseObject.description
                println(resultDesp)

            },
            failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
                println("Error: " + error.description)
                
                actInd.stopAnimating()
                let errorAlert = UIAlertView(title: "Network Error", message: error.localizedDescription, delegate: self, cancelButtonTitle: "OK")
                errorAlert.show()
        })*/
        
    }*/

    
        
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
