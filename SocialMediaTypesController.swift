//
//  SocialMediaTypesController.swift
//  Vyomo
//
//  Created by apple on 08/04/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class SocialMediaTypesController: UIViewController, UIScrollViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Scroll view functions...
    func scrollViewDidScroll(scrollView: UIScrollView!) {
    }
    
    func scrollViewWillEndDragging(scrollView: UIScrollView!, velocity: CGPoint, inout targetContentOffset: CGPoint) {
    }

    //Buttons declaration...
    @IBAction func saveButton(sender: AnyObject) {
    }

    @IBAction func backPressed(sender: AnyObject) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
